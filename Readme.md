# Execution
* Build the docker ```sudo docker build -t container_name file_path```
* ```sudo docker run -d -p 8080:8081 container_name```
  
# Remove the docker container
* ```sudo docker ps```  
* Check the docker container id and the execute following command ```sudo docker kill container_id```
